#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>

struct Date {
	int year;
	int month;
	int day;
};

struct Adress {
	int postIndex;
	char country[30];
	char region[30];
	char district[30];
	char city[30];
	char street[30];
	char build[30];
	char apartment[20];
};

struct Customer {
	char lastname[20];
	char name[20];
	char patronym[20];
	char gender[10];
	Date dateOfBirthday;
	long long phoneNumber;
	Adress adress;
	long long noCreditCard;
	char noBank[50];

};

void addCustomer(Customer* customers, int *N) {
	*N += 1;
	system("cls");

	printf("lastname = ");
	scanf("%s", customers[*N - 2].lastname);
	printf("name = ");
	scanf("%s", customers[*N - 2].name);
	printf("patronym = ");
	scanf("%s", customers[*N - 2].patronym);
	printf("gender = ");
	scanf("%s", customers[*N - 2].gender);

	printf("\ndate of birthday:\n");
	printf("year = ");
	scanf("%d", &customers[*N - 2].dateOfBirthday.year);
	printf("month = ");
	scanf("%d", &customers[*N - 2].dateOfBirthday.month);
	printf("day = ");
	scanf("%d", &customers[*N - 2].dateOfBirthday.day);

	printf("\nphone number = ");
	scanf("%lld", &customers[*N - 2].phoneNumber);

	printf("\nadress:\n");
	printf("post index = ");
	scanf("%d", &customers[*N - 2].adress.postIndex);
	printf("country = ");
	scanf("%s", &customers[*N - 2].adress.country);
	printf("region = ");
	scanf("%s", &customers[*N - 2].adress.region);
	printf("district = ");
	scanf("%s", &customers[*N - 2].adress.district);
	printf("city = ");
	scanf("%s", &customers[*N - 2].adress.city);
	printf("street = ");
	scanf("%s", &customers[*N - 2].adress.street);
	printf("build = ");
	scanf("%s", &customers[*N - 2].adress.build);
	printf("apartment = ");
	scanf("%s", &customers[*N - 2].adress.apartment);

	printf("\nnoCreditCard = ");
	scanf("%lld", &customers[*N - 2].noCreditCard);

	printf("noBank = ");
	scanf("%s", customers[*N - 2].noBank);
}

void printCustomers(Customer* customers, int *N) {
	system("cls");
	printf("     lastname     |       name       |     patronym     |     gender      |  year  | month | day \n");
	for (int i = 0; i < (*N) - 1; i++) {
		printf("%17s | %16s | %16s | %12s | %6d | %5d | %5d \n", customers[i].lastname, customers[i].name, customers[i].patronym, customers[i].gender, customers[i].dateOfBirthday.year, customers[i].dateOfBirthday.month, customers[i].dateOfBirthday.day);
	}
	printf("\nphone number   | postIndex |    country   |   region   |   district   |   city   |   street   |  build  | apartment |  noCreditCard   |  noBank  \n");
	for (int i = 0; i < (*N) - 1; i++) {
		printf("%14lld | %9d | %12s | %10s | %12s | %8s | %10s | %8s | %8s | %15lld | %15s\n", customers[i].phoneNumber, customers[i].adress.postIndex, customers[i].adress.country, customers[i].adress.region, customers[i].adress.district, customers[i].adress.city, customers[i].adress.street, customers[i].adress.build, customers[i].adress.apartment, customers[i].noCreditCard, customers[i].noBank);
	}
	_getch();
}

void search(Customer* customers, int* N, char name[30]) {
	Customer** res = (Customer**)calloc(10, sizeof(Customer*));
	int k = 0;
	int *K = &k;
	for (int i = 0; i < *N; i++) {
		if (strcmp(customers[i].adress.country, name) == 0) {
			res[*K] = &customers[i];
			(*K)++;
		}
	}
	(*K)++;
	printCustomers(*res, K);
}

void deleteN(Customer* customers, int* N, int No) {
	for (int i = No; i < *N; i++) {
		customers[i] = customers[i + 1];
	}
	(*N)--;
}

void sort(Customer* customers, int* N) {
	int fl;
	Customer sl;
	do {
		fl = 0;
		for (int i = 1; i < *N - 1; i++)
			if (strcmp(customers[i - 1].adress.country, customers[i].adress.country) > 0) {
				sl = customers[i - 1];
				customers[i - 1] = customers[i];
				customers[i] = sl;
				fl = 1;
			}
	} while (fl);
}

void menu(Customer* customers, int* N) {
	int choose;
	do {
		system("cls");

		printf("1 -- ������ �������\n2 -- �������� �������\n3 -- ������� ��������\n4 -- ������� ������� � �������� ��������� ���� \"�����\"\n5 -- ����������� �������� �� ����� \"�����\"\n0 -- �����\n\n");

		scanf("%d", &choose);

		switch (choose) {
		case 1:
			addCustomer(customers, N);
			break;
		case 2:
			int No;
			printf("������ ����� ��������, �� ������� �������� ");
			scanf("%d", &No);
			deleteN(customers, N, 0);
			break;
		case 3:
			printCustomers(customers, N);
			break;
		case 4:
			char name[30];
			printf("������ ����� ����� ");
			scanf("%s", name);
			search(customers, N, name);
			break;
		case 5:
			sort(customers, N);
			break;
		case 0:
			return;
		default:
			printf("������� ����������� ��������");
			break;
		}
	} while (choose != 0);
}

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n = 1;
	int *N = &n;
	Customer* customers = (Customer*)calloc(10, sizeof(Customer));
	
	menu(customers, N);

	return 0;
}